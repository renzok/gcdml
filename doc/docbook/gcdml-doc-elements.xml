<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="./rng/docbookxi.rng" type="xml"?>
<sect1 xmlns="http://docbook.org/ns/docbook" xmlns:xi="http://www.w3.org/2001/XInclude"
    xml:id="structure">
    <title>Structure of GCDML reports</title>
    <para>The structure of the XML document is shown in Example 2.1 (note that only elements that
        are mandatory for all types of reports (eukaryotic, prokaryotic, etc.) are shown in this
        example)</para>
    <example>
        <title>Basic structure of all MIGS/MIMS compliant reports in GCDML</title>
        <programlisting><xi:include href="./snips/skeleton.xml" parse="text"/></programlisting>
    </example>
    <para>&lt;nasReports/&gt; is a container element which can have a set of MIGS reports or
        of GCD reports, or both.</para>
    <sect2>
        <title>MIGS/MIMS only reports</title>
        <para>The reports within &lt;MIGSReports&gt; are MIGS/MIMS only compliant reports.
            They implement the MIGS checklist 2.0 as specified in Table 2.1. Following is the
            description of elements of &lt;MIGSReports&gt;</para>
        <sect3>
            <title>Children of &lt;MIGSReports&gt;</title>
            <orderedlist>
                <listitem>
                    <para> &lt;eukaryote&gt; - Eukaryote report</para>
                </listitem>
                <listitem>
                    <para> &lt;prokaryote&gt; - Prokaryote report</para>
                </listitem>
                <listitem>
                    <para> &lt;plasmid&gt; - Plasmid report</para>
                </listitem>
                <listitem>
                    <para> &lt;virus&gt; - Virus report</para>
                </listitem>
                <listitem>
                    <para> &lt;organelle&gt; - Organelle report</para>
                </listitem>
                <listitem>
                    <para> &lt;metagenome&gt; - Metagenome report</para>
                </listitem>
            </orderedlist>
        </sect3>
        <sect3>
            <title>Attributes of &lt;MIGSReports&gt;</title>
            <para>All attributes are mandatory</para>
            <orderedlist>
                <listitem>
                    <para> gcatID - Either an identifier for the Genome Catalog application (e.g.
                        000485_GCAT) or String "notAssigned to indicate that entry does not have
                        this identifier</para>
                </listitem>
                <listitem>
                    <para> sourceName - The source of the report</para>
                </listitem>
                <listitem>
                    <para> sourceVersion - The version number of the database from which the data
                        was gathered.</para>
                </listitem>
            </orderedlist>
        </sect3>
        <sect3>
            <title>Children of individual reports</title>
            <para>Here children of individual reports are listed alongside with their respective
                children elements. A notable diversion from the MIGS checklist is the presence of
                three children elements which are featured at the beginning of each report but are
                not specified in the MIGS checklist:</para>
            <orderedlist>
                <listitem>
                    <para>ncbiOrganismName - a mandatory organism name imported from NCBI Genome
                        Project database. Should be consitent with NCBI taxonomy name.</para>
                </listitem>
                <listitem>
                    <para>ncbiTaxID - optional taxid imported from the NCBI Genome Projects
                        database.</para>
                </listitem>
                <listitem>
                    <para>genomeProjectID - optional Genome Project Identifier</para>
                </listitem>
            </orderedlist>
            <para>The following children are in accordance to the MIGS checklist. Note that for each
                element, all its possible children are listed. To see which of these are actually
                necessary, consult the MIGS checklist for the report in question.</para>
            <orderedlist>
                <listitem>
                    <para> &lt;studyData&gt; - Metadata about the study.</para>
                    <orderedlist>
                        <listitem>
                            <para>&lt;projectName&gt; - name of the project</para>
                        </listitem>
                    </orderedlist>
                </listitem>
                <listitem>
                    <para> &lt;originalSample&gt; - all elements necessary to describe the
                        environmental origin of the very first sample material collected in the
                        field.</para>
                    <orderedlist>
                        <para>One of the following:</para>
                        <listitem>
                            <para> &lt;physicalMaterial&gt; </para>
                            <orderedlist>
                                <listitem>
                                    <para> &lt;amount&gt; - The volume of soil, water etc
                                        used in a metagenomic study. </para>
                                </listitem>
                                <listitem>
                                    <para> &lt;samplingTime&gt; - The time of sampling.
                                        Either as instance (single point in time) or interval. In
                                        case no exact time is available. The date/time can be right
                                        truncated i.e. all of these are valid times:
                                        2008-01-23T19:23:10+00:00; 2008-01-23T19:23:10; 2008-01-23;
                                        2008-01-23; 2008-01; 2008; Except: 2008-01; 2008 all are
                                        ISO6801 compliant. </para>
                                </listitem>
                                <listitem>
                                    <para> &lt;Location&gt;</para>
                                    <orderedlist>
                                        <listitem>
                                            <para> &lt;name&gt; - name of the location from
                                                where the sample was taken </para>
                                        </listitem>
                                        <listitem>
                                            <para> &lt;lat&gt; - lattitude implemented as a
                                                double precision number</para>
                                        </listitem>
                                        <listitem>
                                            <para> &lt;lon&gt; - longitude implemented as a
                                                double precision number</para>
                                        </listitem>
                                    </orderedlist>
                                </listitem>
                                <listitem>
                                    <para> &lt;_Habitat&gt; - the head of a substitution
                                        group which may contain any element that serves as a
                                        container for environmental parameters. Note that the
                                        _Habitat substitution group just serves as a container for
                                        measurable environmental parameters and by no means try to
                                        mimic any biological knowledge about the concept of habitat.
                                        It is just a syntactic construct. For possible substitution
                                        values see below</para>
                                </listitem>
                            </orderedlist>
                        </listitem>
                        <listitem>
                            <para> &lt;organismalMaterial&gt; - contains all the children of
                                &lt;physicalMaterial&gt; as well as:</para>
                            <orderedlist>
                                <listitem>
                                    <para> &lt;name&gt; </para>
                                    <orderedlist>
                                        <listitem>
                                            <para> &lt;genus&gt; - implemented as a name
                                                with first letter upper case and minimum two
                                                characters </para>
                                        </listitem>
                                        <listitem>
                                            <para> &lt;species&gt; - implemented as a name
                                                with first letter lower case and minimum two
                                                characters</para>
                                        </listitem>
                                        <listitem>
                                            <para> &lt;strain&gt; - implemented as a
                                            token</para>
                                        </listitem>
                                        <listitem>
                                            <para> &lt;typeStrain&gt; - implemented as a
                                                boolean true/false value</para>
                                        </listitem>
                                    </orderedlist>
                                </listitem>
                                <listitem>
                                    <para> &lt;healthDiseaseStatus&gt; - health or disease
                                        status of specific host at time of collection. Possible CV
                                        values: alive, asymtomatic.</para>
                                </listitem>
                            </orderedlist>
                        </listitem>
                        <listitem>
                            <para> &lt;collection&gt; - either
                                &lt;physicalMaterial&gt; or
                                &lt;organismalMaterial&gt;; contains a mandatory attribute
                                &lt;criteria&gt; implemented as a token.</para>
                        </listitem>
                    </orderedlist>
                </listitem>
                <listitem>
                    <para> &lt;isolate&gt; - the data about a culture obtained by
                        cultivation</para>
                    <orderedlist>
                        <listitem>
                            <para>&lt;litReference&gt; - general literature reference list
                                in the form of consecutive elements, each containing a DOI or a
                                PubMedID</para>
                        </listitem>
                        <listitem>
                            <para>&lt;isolationConditions&gt; - implemented as a reference
                                list in the form of consecutive elements, each containing a DOI or a
                                PubMedID</para>
                        </listitem>
                        <listitem>
                            <para>&lt;cultureCollection&gt; - has to occur one or many
                                times; has one of the following children</para>
                            <orderedlist>
                                <listitem>
                                    <para>na - not available</para>
                                </listitem>
                                <listitem>
                                    <para>identifier - implemented as a string</para>
                                </listitem>
                            </orderedlist>
                        </listitem>
                        <listitem>
                            <para>&lt;subspecificGeneticLineage&gt; - this should provide
                                further information about the genetic distinctness of this lineage
                                by recording additional information i.e biovar, serovar, serotype,
                                biovar, or any relevant genetic typing schemes like Group I plasmid.
                                Could also contain alternative taxonomic information. Input: cv in
                                GCat</para>
                        </listitem>
                        <listitem>
                            <para>&lt;trophicLevel&gt; - trophic levels are the feeding
                                position in a food chain such as primary producers, herbivore,
                                primary carnivore, etc. Photosynthetic organisms form the first
                                trophic level, the producers. Herbivores form the second trophic
                                level, while carnivores form the third and even the fourth trophic
                                levels. Microbes can be a range of producers (e.g. chemolithotroph).
                                Input: CV</para>
                        </listitem>
                        <listitem>
                            <para>&lt;numReplicons&gt; - an integer reporting the number of
                                replicons in a nuclear genome of eukaryotes, in the genome of a
                                bacterium or archaea or the number of segments in a segmented virus.
                                Always applied to the haploid chromsome count of a eukaryote.</para>
                        </listitem>
                        <listitem>
                            <para>&lt;bioticRelationship&gt; - is it free-living on in a
                                host and if the latter what type of relationship is observed (e.g.
                                pathogen, commensal, symbiont, opportunist). Input CV</para>
                        </listitem>
                        <listitem>
                            <para>&lt;oxygenRelation&gt; - is this organism an aerobe,
                                anaerobe etc. Input CV</para>
                        </listitem>
                        <listitem>
                            <para>&lt;ploidyLevel&gt; - the ploidy level of the genome (e.g.
                                allopolyploid, haploid, diploid etc). Has implications for the
                                downstream study of duplicated gene and regions of the genomes (and
                                perhaps for difficulties in assembly). Input: CV</para>
                        </listitem>
                        <listitem>
                            <para>&lt;extrachromosomalElements&gt; - do plasmids exist of
                                significant phenotypic consequence (e.g. ones that determine
                                virulence or antibiotic resistence).</para>
                        </listitem>
                        <listitem>
                            <para>&lt;estimatedSize&gt; - the estimated size of the genome
                                prior to sequencing. Of particular importance in the sequencing of
                                (eukaryotic) genome which could remain in draft form for a long or
                                unspecified period. Input: integer, size in bp. Example: 300000
                                Example: 4567</para>
                        </listitem>
                        <listitem>
                            <para>&lt;originalHost&gt; - if there is a host involved, its
                                taxid (or environmental is not actually isolated from the dead or
                                alive host - i.e. pathogen could be isolated from a swipe of a bench
                                etc) should be provided and the fact whether it is a laboratory or
                                natural host should be reported. From this we can calculate any
                                number of groupings of hosts (e.g. animal vs plant, all fish hosts,
                                etc)</para>
                        </listitem>
                        <listitem>
                            <para>&lt;alternateHost&gt; - any (zero or more) number of
                                alternative hosts implemented in the same way as
                                &lt;originalHost&gt;</para>
                        </listitem>
                        <listitem>
                            <para>&lt;hostSpecificityAndRange&gt; - if there is a host
                                involved, what is its presumed host specificity and range. This is a
                                highly debatably concept and often this information is not known or
                                has not been properly observed; for example narrow or broad
                                host-range plasmid. Input: taxid</para>
                        </listitem>
                        <listitem>
                            <para>&lt;healthDiseaseStatus&gt; - health or disease status of
                                specific host at time of collection</para>
                        </listitem>
                        <listitem>
                            <para>&lt;pathogenicity&gt; - to what is the entity
                            pathogen</para>
                        </listitem>
                        <listitem>
                            <para>&lt;propagation&gt; - describes the way of propagation of
                                the organism. Input: CV</para>
                        </listitem>
                    </orderedlist>
                </listitem>
                <listitem>
                    <para> &lt;nucExtract&gt; - How was the genetic (DNA or RNA) material
                        extracted and prepared for library construction (e.g. MDA, emPCR, plones)</para>
                    <orderedlist>
                        <listitem>
                            <para> &lt;method&gt; - method of extraction, input: CV; e.g.
                                formaldihide/CTAB extraction, Phenol/Chloroform</para>
                        </listitem>
                        <listitem>
                            <para>&lt;litReference&gt; - literature reference (DOI or
                                PubMedID)</para>
                        </listitem>
                    </orderedlist>
                </listitem>
                <listitem>
                    <para> &lt;dnaLibrary&gt; - data about the library either constructed
                        for a genome or metagenome project</para>
                    <orderedlist>
                        <listitem>
                            <para> &lt;litReference&gt; - literature reference (DOI or
                                PubMedID </para>
                        </listitem>
                        <listitem>
                            <para> &lt;librarySize&gt; - total number of clones in library.
                                Input: integer</para>
                        </listitem>
                        <listitem>
                            <para> &lt;numSequencedClones&gt; - total number of clones
                                sequenced for this project. Input: integer </para>
                        </listitem>
                        <listitem>
                            <para> &lt;cloneVector&gt; - e.g. pUC19. Possesses an optional
                                attribute &lt;sequenceAccession&gt; </para>
                        </listitem>
                    </orderedlist>
                </listitem>
                <listitem>
                    <para> &lt;sequencing&gt; - metadata about the sequencing and applied
                        data processing (Note: It does not contain the DNA or protein sequences
                        themselves)</para>
                    <orderedlist>
                        <listitem>
                            <para>&lt;sequencingMethod&gt; - input: CV</para>
                        </listitem>
                        <listitem>
                            <para>&lt;assembly&gt; - input: CV</para>
                        </listitem>
                        <listitem>
                            <para>&lt;sops&gt; - Standard operating procedures</para>
                        </listitem>
                    </orderedlist>
                </listitem>
            </orderedlist>
        </sect3>
        <!-- <orderedlist>-->

        <!--
            <listitem>
                <para>&lt;originalSample/&gt; contains all elements necessary to describe
                    the environmental origin of the very first sample material collected in the
                    field including</para>
                <orderedlist>
                    <listitem>
                        <para>&lt;samplingTime/&gt; the time of sampling</para>
                    </listitem>
                    <listitem>
                        <para>&lt;_SampleLocation/&gt; a substitution for any kind of
                            geographic description of the source sample</para>
                    </listitem>
                    <listitem>
                        <para>&lt;_Habitat/&gt; is a substitution for any kind of xml
                            element which contains data about the habitat from where a sample was
                            taken (Note: &lt;_Habitat/&gt; is purely technically defined
                            w.r.t. the differences in descriptors needed to describe the
                        environment</para>
                    </listitem>
                </orderedlist>
            </listitem>
        </orderedlist>-->
    </sect2>
</sect1>
