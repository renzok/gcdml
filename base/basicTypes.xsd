<?xml version="1.0" encoding="UTF-8"?>
<schema targetNamespace="http://gensc.org/gcdml" xmlns:gcdml="http://gensc.org/gcdml"
    xmlns:gml="http://www.opengis.net/gml" xmlns="http://www.w3.org/2001/XMLSchema" version="1.7.0"
    elementFormDefault="qualified">
    <annotation>
        <documentation xml:lang="en"
            >Generic type (simple and complex) components for use in GCDML
        </documentation>
    </annotation>

    <include schemaLocation="./simpleTypes.xsd" />
    <include schemaLocation="./cvTypes.xsd" />

    <!--    <import namespace="http://www.opengis.net/gml"
        schemaLocation="http://schemas.opengis.net/gml/3.1.1/base/gml.xsd" />
    -->
    <!-- <import namespace="http://www.opengis.net/gml" schemaLocation="../gml/3.1.1/base/gml.xsd"/> -->

    <import namespace="http://www.opengis.net/gml"
        schemaLocation="../gml/3.1.1/profiles/gmlJP2Profile/1.0.0/gmlJP2Profile.xsd" />

    <element name="na">
        <complexType>
            <complexContent>
                <restriction base="anyType">
                    <attribute name="reason" type="gcdml:NullEnumeration" />
                </restriction>
            </complexContent>
        </complexType>
    </element>

    <!-- ============================================================== -->
    <!-- == Extension ================================================== -->
    <!-- ============================================================== -->

    <element name="extension">
        <annotation>
            <documentation> Extension of reports to allow inclusion of additional elements from the
                same or different namespace</documentation>
        </annotation>
        <complexType>
            <sequence>
                <any namespace="##targetNamespace" processContents="strict" minOccurs="0"
                    maxOccurs="unbounded" />
                <any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded"
                 />
            </sequence>
        </complexType>
    </element>

    <!-- ============================================================== -->
    <!-- == Reference Types ================================================== -->
    <!-- ============================================================== -->


    <element name="link" type="gcdml:linkType">
        <annotation>
            <documentation xml:lang="en"
                >A link to some other web document. Only an URI not an URL.
                Handling of the URI is left to the application.</documentation>
        </annotation>
    </element>

    <simpleType name="linkType">
        <annotation>
            <documentation xml:lang="en">Standard operating procedures</documentation>
        </annotation>
        <union memberTypes="gcdml:NullEnumeration">
            <simpleType>
                <restriction base="anyURI" />
            </simpleType>
        </union>
    </simpleType>



    <simpleType name="sopType">
        <annotation>
            <documentation xml:lang="en">Standard operating procedures</documentation>
        </annotation>
        <union memberTypes="gcdml:NullEnumeration">
            <simpleType>
                <restriction base="string" />
            </simpleType>
        </union>
    </simpleType>

    <!-- ============================================================== -->
    <!-- == Identifier Types ================================================== -->
    <!-- ============================================================== -->


    <simpleType name="GcatIdType">
        <annotation>
            <documentation xml:lang="en"
                >Either an identifier for the Genome Catalog application
                (e.g. 000485_GCAT) or String "notAssigned to indicate that entry does not have this
                identifier</documentation>
        </annotation>
        <union memberTypes="gcdml:notAssigned">
            <simpleType>
                <restriction base="string">
                    <pattern value="\d{6}_GCAT">
                        <annotation>
                            <documentation xml:lang="en"
                                >Regular expression matches against genome
                                catalog identifier e.g. 000485_GCAT</documentation>
                        </annotation>
                    </pattern>
                </restriction>
            </simpleType>
        </union>
    </simpleType>

    <simpleType name="sourceNameType">
        <annotation>
            <documentation xml:lang="en">The source of the report</documentation>
        </annotation>
        <union memberTypes="gcdml:notAssigned">
            <simpleType>
                <restriction base="anyURI" />
            </simpleType>
        </union>
    </simpleType>

    <element name="ncbiTaxId" type="gcdml:ncbiTaxIdType" />

    <simpleType name="ncbiTaxIdType">
        <annotation>
            <documentation>NCBI taxonomy identifier. If the organism
                does not yet have a taxonomy identifier one will be created the first time a molecular
                sequence is submitted to one of the INSDC databases.</documentation>
        </annotation>
        <union memberTypes="gcdml:NullEnumeration">
            <simpleType>
                <restriction base="positiveInteger">
                    <whiteSpace value="collapse" />
                    <totalDigits value="10" />
                </restriction>
            </simpleType>
        </union>
    </simpleType>


    <simpleType name="AccessionNumberType">
        <annotation>
            <documentation>The accession number to resolve all replicons in this entry in the INSDC
                databases (DDBJ/EMBL/Genbank)</documentation>
        </annotation>
        <restriction base="string">
            <annotation>
                <documentation xml:lang="en"
                    >Pattern matches GenBank accession numbers with single
                    letter followed by five digits (e.g., U12345) or two letters followed by six
                    digits (e.g., AF123456).</documentation>
            </annotation>
            <pattern value="(\w\d{5})|(\w{2}\d{6})" />
        </restriction>
    </simpleType>

    <!-- used internally -->
    <simpleType name="AccessionNumberListType">
        <annotation>
            <documentation xml:lang="en">List of Accession numbers</documentation>
        </annotation>
        <list itemType="gcdml:AccessionNumberType" />
    </simpleType>

    <element name="genomeProjectID" type="gcdml:GenomeProjectIdType" />

    <simpleType name="GenomeProjectIdType">
        <annotation>
            <documentation>Please enter the Genome Project Identifier for the  genome, if available from the INSDC. Otherwise chose "not assigned".</documentation>
        </annotation>
        <union>
            <simpleType>
                <restriction base="positiveInteger">
                    <whiteSpace value="collapse" />
                    <totalDigits value="10" />
                </restriction>
            </simpleType>
            <simpleType>
                <restriction base="string">
                    <enumeration value="not assigned" />
                </restriction>
            </simpleType>
        </union>
    </simpleType>


    <!-- used internally -->
    <element name="goldStamp" type="gcdml:GoldStampType" />

    <simpleType name="GoldStampType">
        <annotation>
            <documentation>Identifier imported from the Genomes Online Database.</documentation>
        </annotation>
        <restriction base="string">
            <whiteSpace value="collapse" />
            <pattern value="Gc\d{1,8}" />
        </restriction>
    </simpleType>

    <simpleType name="PubMedID">
        <annotation>
            <documentation xml:lang="en"
                >The identifier assigned by
            http://www.pubmed.com</documentation>
        </annotation>
        <restriction base="positiveInteger">
            <whiteSpace value="collapse" />
            <totalDigits value="10" />
        </restriction>
    </simpleType>

    <element name="doi" type="gcdml:DoiType">
        <annotation>
            <documentation>The Document Object Identifier</documentation>
        </annotation>
    </element>

    <simpleType name="DoiType">
        <annotation>
            <documentation xml:lang="en">The Document Object Identifier</documentation>
        </annotation>
        <restriction base="string">
            <pattern value=".*/.*" />
        </restriction>
    </simpleType>

    <element name="lsid" type="gcdml:LsidType" />

    <simpleType name="LsidType">
        <annotation>
            <documentation xml:lang="en">Life Science Identifier</documentation>
        </annotation>
        <restriction base="string" />
    </simpleType>

    <element name="biomaterialReference" type="gcdml:BioMaterialReferenceType" />

    <complexType name="BioMaterialReferenceType">
        <sequence>
            <element ref="gcdml:doi" minOccurs="0" maxOccurs="unbounded" />
            <element ref="gcdml:lsid" minOccurs="0" maxOccurs="unbounded" />
            <element ref="gcdml:link" minOccurs="0" maxOccurs="unbounded" />
        </sequence>
    </complexType>



    <!-- ============================================================== -->
    <!-- == Literature References ============================================== -->
    <!-- ============================================================== -->

    <element name="_Publication" abstract="true" />

    <element name="article" type="gcdml:FullArticleType" substitutionGroup="gcdml:_Publication" />

    <element name="litReference" type="gcdml:litReferenceType"
        substitutionGroup="gcdml:_Publication">
        <annotation>
            <documentation xml:lang="en">Reference to literature</documentation>
        </annotation>
    </element>

    <complexType name="litReferenceType">
        <choice>
            <choice maxOccurs="unbounded">
                <element ref="gcdml:doi" />
                <element ref="gcdml:pmid" />
            </choice>
            <element ref="gcdml:na" />
        </choice>
    </complexType>

    <element name="pmid" type="gcdml:PubMedID">
        <annotation>
            <documentation xml:lang="en"
                > The PubMed identifier (http://www.pubmed.com) of an
                article </documentation>
        </annotation>
    </element>

    <complexType name="FullArticleType">
        <sequence>
            <element name="title" type="gcdml:MinStringType" />
            <element name="author" type="gcdml:AuthorType" maxOccurs="unbounded" />
            <element name="year" type="gYear" />
            <element name="vol" type="gcdml:MinStringType" />
            <element name="journal" type="gcdml:MinStringType" />
        </sequence>
        <attribute name="url" type="anyURI">
            <annotation>
                <documentation>The URL of the full text</documentation>
            </annotation>
        </attribute>
        <attribute name="pmid" type="gcdml:PubMedID" use="optional" />
        <attribute name="doi" type="gcdml:DoiType" use="optional" />
    </complexType>

    <!-- used internally

        From NCBI DTD

        <!ENTITY % personal.name "(LastName,(ForeName|(FirstName,MiddleName?))?,
    Initials?,Suffix?)">
    <!ENTITY % author.name "(%personal.name; | CollectiveName)">

    TODO: we can also take the schema elements as defined by NCBI
    -->
    <complexType name="AuthorType">
        <choice>
            <element name="collectiveName" type="gcdml:MinStringType" />
            <element ref="gcdml:author" />
        </choice>
        <attribute name="position" type="positiveInteger" use="optional" />
    </complexType>



    <element name="author" type="gcdml:PersonNameType" />

    <complexType name="PersonNameType">
        <sequence>
            <element name="lastName" type="gcdml:UpperCaseNameType" />
            <sequence minOccurs="0">
                <element name="firstName" type="gcdml:UpperCaseNameType" />
                <element name="middleName" type="gcdml:MinStringType" minOccurs="0" />
            </sequence>
            <element name="initials" type="gcdml:MinStringType" minOccurs="0"> </element>
            <element name="suffix" type="gcdml:MinStringType" minOccurs="0"> </element>
        </sequence>
    </complexType>


    <!-- ============================================================== -->
    <!-- == Time Types ================================================ -->
    <!-- ============================================================== -->

    <simpleType name="FuzzyTimePositionUnion">
        <annotation>
            <documentation xml:lang="en"
                >Dates and dateTime may be indicated with varying degrees of
                precision. dateTime by itself does not allow right-truncation, except for fractions
                of seconds. When used with non-Gregorian calendars based on years, months, days, the
                same lexical representation should still be used, with leading zeros added if the
                year value would otherwise have fewer than four digits. .</documentation>
        </annotation>
        <union memberTypes="gml:CalDate dateTime time " />
    </simpleType>


    <complexType name="FuzzyTimePositionType" final="#all">
        <annotation>
            <documentation xml:lang="en"
                >Direct representation of a temporal position. Based on GML
                3.1.1 Indeterminate time values are also allowed, as described in ISO 19108. The
                indeterminatePosition attribute can be used alone or it can qualify a specific value
                for temporal position (e.g. before 2002-12). For time values that identify position
                within a calendar, the calendarEraName attribute provides the name of the calendar
                era to which the date is referenced (e.g. the Meiji era of the Japanese
            calendar).</documentation>
        </annotation>
        <simpleContent>
            <extension base="gcdml:FuzzyTimePositionUnion">
                <attribute name="frame" type="anyURI" use="optional" default="#ISO-8601" />
                <attribute name="calendarEraName" type="string" use="optional" />
                <attribute name="indeterminatePosition" type="gml:TimeIndeterminateValueType"
                    use="optional" />
            </extension>
        </simpleContent>
    </complexType>

    <complexType name="FuzzyTimeInterval">
        <annotation>
            <documentation>A time interval with inexact </documentation>
        </annotation>
        <sequence>
            <element name="start" type="gcdml:FuzzyTimePositionType" />
            <element name="stop" type="gcdml:FuzzyTimePositionType" />
        </sequence>
    </complexType>

    <element name="samplingTime" type="gcdml:DateTimeType" />

    <complexType name="DateTimeType">
        <annotation>
            <documentation xml:lang="en"
                >The time of sampling. Either as instance (single point in
                time) or interval. In case no exact time is available. The date/time can be right
                truncated i.e. all of these are valid times: 2008-01-23T19:23:10+00:00;
                2008-01-23T19:23:10; 2008-01-23; 2008-01-23; 2008-01; 2008; Except: 2008-01; 2008
                all are ISO6801 compliant. </documentation>
        </annotation>
        <sequence>
            <choice>
                <element name="instance" type="gcdml:FuzzyTimePositionType" />
                <element name="interval" type="gcdml:FuzzyTimeInterval" />
                <element ref="gcdml:na" />
            </choice>
        </sequence>
    </complexType>

    <!-- ============================================================== -->
    <!-- == Enum types ===================================================== -->
    <!-- ============================================================== -->


    <element name="foldCoverage" type="gcdml:foldCoverageType" />

    <simpleType name="foldCoverageType">
        <annotation>
            <documentation>Coverage, the fold coverage of the sequencing expressed as 2x, 3x, 18x
                etc</documentation>
        </annotation>
        <union memberTypes="gcdml:NullEnumeration">
            <simpleType>
                <restriction base="positiveInteger">
                    <maxExclusive value="100" />
                </restriction>
            </simpleType>
        </union>
    </simpleType>

    <element name="fragmentClassificationMethod">
        <annotation>
            <documentation>What, if any, method was used to classify the fragments of the metagenome
                into different groups based on compositional features. Just a
            string.</documentation>
        </annotation>
        <complexType>
            <simpleContent>
                <extension base="string" />
            </simpleContent>
        </complexType>
    </element>


    <simpleType name="GenomeProjectOrganismNameType">
        <annotation>
            <documentation>Organism name imported from NCBI Genome Project database. Should be
                consitent with NCBI taxonomy name.</documentation>
        </annotation>
        <restriction base="string">
            <whiteSpace value="collapse" />
        </restriction>
    </simpleType>

    <element name="numReplicons" type="gcdml:posIntegerOrNull">
        <annotation>
            <documentation>Reports the number of replicons in a nuclear genome of
                eukaryotes, in the genome of a bacterium or archaea or the number of segments in a
                segmented virus. Always applied to the haploid chromsome count of a
            eukaryote.</documentation>
        </annotation>
    </element>

    <element name="extrachromosomalElements" type="gcdml:nonNegIntegerOrNull">
        <annotation>
            <documentation>For eukarya (optional) and bacteria (mimumum), do plasmids exist of
                significant phenotypic consequence (e.g. ones that determine virulence or antibiotic
                resistence). Megaplasmids? Other plasmids (borrelia has 15+
            plasmids)</documentation>
        </annotation>
    </element>

    <element name="estimatedSize" type="gcdml:posIntegerOrNull">
        <annotation>
            <documentation>The estimated size of the genome prior to sequencing. Of particular
                importance in the sequencing of (eukaryotic) genome which could remain in draft form
                for a long or unspecified period. Input: integer, size in bp. Example: 300000
                Example: 4567</documentation>
        </annotation>
    </element>


    <!-- TODO work out the list straininfo.net???? -->
    <simpleType name="CultureCollectionNameEnumType">
        <annotation>
            <documentation>The name of the culture collection, holder of the vocher or an
                institution. Could enumerate a list of common resources, just as the American Type
                Culture Collection (ATCC), German Collection of Microorganisms and Cell Cultures
                (DMZS) etc. Can select not deposited. Input: CV</documentation>
        </annotation>
        <union>
            <simpleType>
                <restriction base="string">
                    <enumeration value="ATCC" />
                    <enumeration value="CCAP" />
                    <enumeration value="DSMZ" />
                    <enumeration value="NCTC" />
                    <enumeration value="Plymouth Virus Collection" />
                </restriction>
            </simpleType>
        </union>
    </simpleType>

    <element name="cultureCollection" type="gcdml:CultureCollectionType" />

    <simpleType name="cultureCollectionNullType">
        <union memberTypes="gcdml:NullEnumeration">
            <simpleType>
                <restriction base="token">
                    <enumeration value="not deposited" />
                    <enumeration value="not publically available" />
                </restriction>
            </simpleType>
        </union>
    </simpleType>


    <complexType name="CultureCollectionMIGSType">
        <choice>
            <element name="na" type="gcdml:cultureCollectionNullType" />
            <sequence>
                <element name="identifier" type="string" />
            </sequence>
        </choice>
    </complexType>

    <complexType name="CultureCollectionType">
        <annotation>
            <documentation>MIGS aims to provide unencumbered access to all genomic materials. All
                genomic regents should be deposited in at least two permanent archives and IDs
                should be made available here (cultures of micro-organisms: identifiers for culture
                collections; specimens (e.g. eukaryara and organelles)</documentation>
        </annotation>
        <sequence>
            <choice>
                <element name="na" type="gcdml:cultureCollectionNullType" />
                <sequence>
                    <element name="name" type="gcdml:CultureCollectionNameEnumType" />
                    <element name="identifier" type="string" />
                    <element name="taxonmicType" type="boolean" />
                    <element name="available" type="boolean" />
                    <element name="viable" type="boolean" />
                    <element name="medium" type="string" />
                </sequence>
            </choice>
        </sequence>
    </complexType>


    <element name="originalHost" type="gcdml:OriginalHostMIGSType">
        <annotation>
            <documentation>If there is a host involved, please provide its taxid (or environmental
                is not actually isolated from the dead or alive host - i.e. pathogen could be
                isolated from a swipe of a bench etc) and report whether it is a laboratory or
                natural host). From this we can calculate any number of groupings of hosts (e.g.
                animal vs plant, all fish hosts, etc)</documentation>
        </annotation>
    </element>

    <complexType name="OriginalHostMIGSType">
        <choice>
            <sequence>
                <element ref="gcdml:ncbiTaxId">
                    <annotation>
                        <documentation>The NCBI taxonomy identifier of the specific host if it is known</documentation>
                    </annotation>
                </element>
                <element ref="gcdml:hostClass" />
                <element ref="gcdml:healthDiseaseStatus" />
                <element ref="gcdml:pathogenicity" minOccurs="0" maxOccurs="unbounded"/>
                <element ref="gcdml:bioticRelationship" minOccurs="0" />
                <element ref="gcdml:cultureCollection" minOccurs="0"/>
            </sequence>
            <element ref="gcdml:na" />
        </choice>
    </complexType>

    <element name="alternateHost" type="gcdml:OriginalHostMIGSType" />

    <element name="hostSpecificityAndRange">
        <annotation>
            <documentation>If there is a host involved, what is its presumed host specificity and
                range. This is a highly debatably concept and often this information is not known or
                has not been properly observed; for example narrow or broad host-range plasmid.
                Input: taxid</documentation>
        </annotation>
        <complexType>
            <simpleContent>
                <extension base="string" />
            </simpleContent>
        </complexType>
    </element>


    <element name="pathogenicity" type="gcdml:PathogenicityType"/>

    <simpleType name="PathogenicityType">
        <annotation>
            <documentation>To what is the entity pathogenic.</documentation>
        </annotation>
        <union memberTypes="gcdml:NullEnumeration">
            <simpleType>
                <restriction base="string">
                    <enumeration value="human" />
                    <enumeration value="animal" />
                    <enumeration value="plants" />
                    <enumeration value="bacteria" />
                    <enumeration value="fungus" />
                    <enumeration value="archea" />
                    <enumeration value="eukaryotes" />
                    <enumeration value="prokaryotes" />
                    <enumeration value="not pathogen" />
                    <enumeration value="inapplicable" />
                </restriction>
            </simpleType>
        </union>
    </simpleType>


    <element name="finishingStrategy" type="gcdml:finishingStrategyType" />

    <simpleType name="finishingStrategyType">
        <annotation>
            <documentation>Was the genome project intended to produce a complete or draft
            genome.</documentation>
        </annotation>
        <union memberTypes="gcdml:NullEnumeration">
            <simpleType>
                <restriction base="string">
                    <enumeration value="complete" />
                    <enumeration value="draft" />
                </restriction>
            </simpleType>
        </union>
    </simpleType>

    <element name="numSequencedClones" type="gcdml:posIntegerOrNull">
        <annotation>
            <documentation>Total number of clones sequenced for this project. Input:
            integer</documentation>
        </annotation>
    </element>


    <simpleType name="CloneVectorType">
        <union memberTypes="gcdml:CloneVectorEnumType string" />
    </simpleType>

    <element name="librarySize" type="gcdml:posIntegerOrNull">
        <annotation>
            <documentation>Total number of clones in library. Input: integer</documentation>
        </annotation>
    </element>


</schema>
