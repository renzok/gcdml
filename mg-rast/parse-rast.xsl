<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://gensc.org/gcdml" exclude-result-prefixes="xs #default" version="2.0">
    
    
    <xsl:output method="xml" indent="yes"/>
    <!-- micropipline according to Michael Kay XSLT 2.0 p. 1060 -->
    <xsl:param name="input" required="yes" as="xs:string"/>
    <!-- I leave out the encoding -->
    <xsl:variable name="input-text" as="xs:string" select="unparsed-text($input)"/>
    <xsl:variable name="input-lines" as="xs:string*" select="tokenize($input-text, '\r?\n')"/>
    <!-- now parsing the lines -->
    <xsl:variable name="line" as="element()*">
        <xsl:for-each select="$input-lines">
            <xsl:analyze-string select="." regex="([A-Za-z_]+)(\t*)(.+)?">
                <xsl:matching-substring>
                    <line key="{regex-group(1)}" value="{regex-group(3)}"/>
                </xsl:matching-substring>
            </xsl:analyze-string>
        </xsl:for-each>
        
    </xsl:variable>

    <xsl:template match="/">
        
        <!--<xsl:call-template name="debug">
            <xsl:with-param name="hello" select="$line"/>
        </xsl:call-template>
        -->
        <xsl:result-document encoding="UTF-8"  >
            <NasReports xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns="http://gensc.org/gcdml" xmlns:gml="http://www.opengis.net/gml">
                <GCDReports>
                    <metagenome gcatID="not assigned" sourceName="mg-rast" sourceVersion="1">
                        <!-- check organism name in metagenome reports -->
                        <ncbiOrganismName/>
                        <genomeProjectID>not assigned</genomeProjectID>
                        <studyData>
                            <xsl:call-template name="project_name"/>
                            <xsl:call-template name="principalinvestigator"/>
                            <email>dlgosdata@venterinstitute.org</email>
                            <website>http://www.jcvi.org/cms/research/projects/gos</website>
                            <affiliation>J. Craig Venter Institute</affiliation>
                        </studyData>

                        <originalSample>
                            <physicalMaterial>
                                <samplingTime>
                                    <instance>20040201</instance>
                                </samplingTime>
                                <!-- bring GPS datum in again -->
                                <location>
                                    <name>134 miles NE of Galapagos</name>
                                    <lat>1.2642</lat>
                                    <lon>-90.295</lon>
                                </location>
                                <habitat>
                                    <aquatic>
                                        <waterBody>
                                            <depth>
                                                <!-- change depth to the same like all other parameters -->
                                                <measure values="2" uom="m"/>
                                            </depth>
                                            <!-- temperature does not values as required -->
                                            <temperature values="" uom="C"/>
                                            <salinity values="32.6" uom="PSU"/>


                                        </waterBody>
                                    </aquatic>

                                </habitat>
                            </physicalMaterial>

                        </originalSample>
                        <nucExtract>
                            <method>placeholder</method>
                        </nucExtract>
                        <sequencing>
                            <sequences/>
                        </sequencing>

                    </metagenome>

                </GCDReports>
            </NasReports>
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template name="principalinvestigator">
        <xsl:if test="$line/@value[../@key = 'lastname']">
            <principalInvestigator>
                <lastName>
                    <xsl:value-of select="$line/@value[../@key = 'lastname']"/>
                </lastName>
                <firstName>Craig</firstName>
                <middleName>J.</middleName>
            </principalInvestigator>
        </xsl:if>
    </xsl:template>


    <xsl:template name="project_name" exclude-result-prefixes="#default">
        <xsl:if test="$line/@value[../@key = 'project_name']">
            <projectNamme>
                <xsl:value-of select="$line/@value[../@key = 'project_name']"/>
            </projectNamme>
        </xsl:if>
    </xsl:template>

    <xsl:template name="debug">
        <xsl:param name="hello" as="element()*"/>        
        
        <xsl:for-each select="$hello">
            <xsl:element name="{@key}">
                <xsl:value-of select="@value"/>
            </xsl:element>
        </xsl:for-each>
        
    </xsl:template>

</xsl:stylesheet>
