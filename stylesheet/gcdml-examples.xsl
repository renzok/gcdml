<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:gcdml="http://gensc.org/gcdml" version="2.0">

    <xsl:param name="outfilepath">../doc/docbook/snips/</xsl:param>

    <xsl:output method="text" encoding="UTF-8" name="text-out" />
    <xsl:output method="xml" encoding="UTF-8" xml:space="default" omit-xml-declaration="yes"
        name="xml-out" />

    <xsl:template match="/gcdml:examples/*">
        <xsl:result-document format="xml-out" href="{$outfilepath}/{ local-name() }.txt">
            <xsl:copy-of select="." xml:space="default" />
        </xsl:result-document>
    </xsl:template>

</xsl:stylesheet>
