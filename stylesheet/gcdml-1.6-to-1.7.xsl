<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:g="http://gensc.org/gcdml" exclude-result-prefixes="g"
    xmlns="http://gensc.org/gcdml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:output method="xml" encoding="UTF-8" />

    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" />
        </xsl:copy>
    </xsl:template>

    <xsl:template match="g:lat[parent::g:Location and number(./text() = 0)]|g:lon[parent::g:Location and number(./text() = 0)]">
        <xsl:if test="contains(comment(),na)">
            <xsl:element name="{local-name(.)}">
                <xsl:text>missing</xsl:text>
            </xsl:element>
        </xsl:if>
    </xsl:template>


    <!-- changing element order in GCDReports/*/physicalMaterial -->
    <xsl:template match="g:physicalMaterial[ancestor::g:GCDReports]">
        <xsl:copy>
            <xsl:apply-templates select="g:samplingTime" />
            <xsl:apply-templates select="g:Location" />
            <xsl:apply-templates select="g:amount" />
            <xsl:apply-templates select="g:habitat" />
            <xsl:apply-templates
                select="g:materialType" />
            <xsl:apply-templates select="g:extension" />
        </xsl:copy>
    </xsl:template>

    <!-- no empty materialtype elements any more -->
    <xsl:template match="g:materialType[string-length(text()) = 0]">
        <xsl:copy>
            <xsl:apply-templates select="@*" />
            <xsl:text>placeholder</xsl:text>
        </xsl:copy>
    </xsl:template>

    <!--  Where not available, add   <genomeProjectID>not assigned</genomeProjectID>-->
    <xsl:template match="g:originalSample[not(../g:genomeProjectID)]">
        <xsl:element name="genomeProjectID">not assigned</xsl:element>
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" />
        </xsl:copy>
    </xsl:template>


    <!-- add mandatory foldCoverage and numContigs to MIGS reports -->
    <xsl:template
        match="g:sequencing[parent::g:prokaryote and ancestor::g:MIGSReports]|g:sequencing[parent::g:eukaryote]">
        <!--        <xsl:text >Hallo</xsl:text>-->
        <xsl:copy>
            <xsl:apply-templates />
            <xsl:element name="foldCoverage">
                <xsl:text>placeholder</xsl:text>
            </xsl:element>
            <xsl:element name="numContigs">
                <xsl:text>placeholder</xsl:text>
            </xsl:element>
        </xsl:copy>

    </xsl:template>


    <!-- from a simple assembly element create a more complex one -->
    <xsl:template match="g:assembly[parent::g:sequencing and ancestor::g:MIGSReports]">
        <xsl:element name="assembly">
            <xsl:element name="assemblyMethod">
                <xsl:value-of select="./text()" />
            </xsl:element>
            <xsl:element name="estimatedErrorRate">
                <xsl:text>placeholder</xsl:text>
            </xsl:element>
            <xsl:element name="calculationMethod">
                <xsl:text>placeholder</xsl:text>
            </xsl:element>
        </xsl:element>
        <xsl:apply-templates />
    </xsl:template>

    <xsl:template match="g:assembly/g:estimatedErrorRate[string-length(text()) = 0 ]">
        <xsl:element name="estimatedErrorRate">
            <xsl:text>placeholder</xsl:text>
        </xsl:element>
    </xsl:template>

    <!-- Do not allow empty depth and amount elements anymore, therefore add na -->
    <xsl:template match="g:amount/g:measure[count(node()) = 0]|g:depth/g:measure[count(node()) = 0]">
        <xsl:element name="na" />
    </xsl:template>

    <!-- Changing GCDReports -->
    <xsl:template
        match="g:depth[ancestor::g:GCDReports and string-length(text()) = 0 ]|g:amount[ancestor::g:GCDReports and string-length(text()) = 0 ]">

        <xsl:if test="not(count(attribute::node()) = 0)">
            <xsl:element name="{local-name(.)}">
                <xsl:element name="measure">
                    <xsl:copy-of select="@*" />
                </xsl:element>
            </xsl:element>
        </xsl:if>

        <xsl:if test="count(attribute::node()) = 0">
            <xsl:element name="{local-name(.)}">
                <xsl:element name="na"> </xsl:element>
            </xsl:element>
        </xsl:if>
    </xsl:template>

    <xsl:template match="g:assembly[parent::g:sequencing]/text()">
        <!-- do nothing = drop text node -->
    </xsl:template>

    <!-- rename specificHostClass to hostClass -->
    <xsl:template match="g:specificHostClass">
        <xsl:element name="hostClass">
            <xsl:apply-templates />
        </xsl:element>
    </xsl:template>

    <!-- rename chlorophyl to chlorophyll -->
    <xsl:template match="g:chlorophyl">
        <xsl:element name="chlorophyll">
            <xsl:apply-templates select="@*|node()" />
        </xsl:element>
    </xsl:template>
    
    <!-- rename sops to sop -->
    <xsl:template match="g:sops[parent::g:sequencing]">
        <xsl:element name="sop">
            <xsl:apply-templates />
        </xsl:element>
    </xsl:template>

    <!-- lower case location element name for consistency -->
    <xsl:template match="g:Location">
        <location>
            <xsl:apply-templates />
        </location>
    </xsl:template>


    <!-- rename element ncbiTaxID and ncbiTaxid to ncbiTaxId -->
    <xsl:template
        match="g:ncbiTaxID|g:ncbiTaxid[parent::g:originalHost]|g:ncbiTaxid[parent::g:alternateHost]">
        <xsl:element name="ncbiTaxId">
            <xsl:apply-templates />
        </xsl:element>
    </xsl:template>

</xsl:stylesheet>
<!-- remove tag only 
<xsl:template match="ins">
    <xsl:apply-templates/>
</xsl:template>

 remove element 
<xsl:template match="del">
</xsl:template>

 insert 
<xsl:template match="list">
    <before>inserted before list</before>
    <xsl:copy>
        <xsl:apply-templates select="@*"/>
        <beginlist>inserted at beginning of list</beginlist>
        <xsl:apply-templates/>
        <endlist>inserted at end of list</endlist>
    </xsl:copy>
    <after>inserted before list</after>
</xsl:template>

 rename element
<xsl:template match="item">
    <myitem>
        <xsl:apply-templates/>
    </myitem>
</xsl:template>

 identity template 
<xsl:template match="@*|node()">
    <xsl:copy>
        <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
</xsl:template>

rename attribute
<xsl:template match="elementToMatch">
<xsl:copy>
<xsl:copy-of select="@*[not(local-name() = 'originalName')]"/>
<xsl:attribute name="newName"><xsl:value-of
    select="@originalName"/></xsl:attribute>
<xsl:apply-templates/>
</xsl:copy>
</xsl:template>



-->
