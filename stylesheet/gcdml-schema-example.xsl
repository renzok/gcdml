<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://www.w3.org/2001/XMLSchema" version="2.0"
    xpath-default-namespace="http://www.w3.org/2001/XMLSchema">
    <xsl:param name="outfilepath">../doc/docbook/snips/</xsl:param>

    <xsl:output method="text" encoding="UTF-8" name="text-out" />
    <xsl:output method="xml" encoding="UTF-8" xml:space="default" omit-xml-declaration="yes"
        name="xml-out" />

    <xsl:template match="*">
        <xsl:message>
            <xsl:text>H3</xsl:text>
        </xsl:message>
        <xsl:apply-templates />
    </xsl:template>


    <xsl:template match="simpleType[@name='bioticRelationshipEnumType']">
        <xsl:message>
            <xsl:text>S2</xsl:text>
        </xsl:message>
        <xsl:result-document format="xml-out" href="{$outfilepath}/{ attribute::name }-xsd.xml">
            <xsl:copy-of select="." xml:space="default" copy-namespaces="no" />
        </xsl:result-document>
    </xsl:template>

</xsl:stylesheet>
